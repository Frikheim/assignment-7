package com.noroff.assignment7.DatabaseIntializer;

import com.noroff.assignment7.Models.Character;
import com.noroff.assignment7.Models.Franchise;
import com.noroff.assignment7.Models.Movie;
import com.noroff.assignment7.Repositories.CharacterRepository;
import com.noroff.assignment7.Repositories.FranchiseRepository;
import com.noroff.assignment7.Repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.HashSet;

@Component
public class AppStartRunner  implements ApplicationRunner {

    @Autowired
    CharacterRepository characterRepository;
    @Autowired
    FranchiseRepository franchiseRepository;
    @Autowired
    MovieRepository movieRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Franchise DC = new Franchise("DC", "Movies about superheros");
        Movie batman = new Movie("Batman", "Action", "2004", "Director", "pictureURL", "trailerURL");
        Movie batman2 = new Movie("Batman Returns", "Action", "2005", "Director", "pictureURL", "trailerURL");
        Character bruce = new Character("Bruce Wayne", "Batman", "Male", "pictureURL");
        Character joker = new Character("Unknown", "The Joker", "Male", "pictureURL");
        HashSet<Character> characters= new HashSet<>();
        characters.add(bruce);
        characters.add(joker);
        batman.setCharacters(characters);
        batman.setFranchise(DC);

        Movie istid = new Movie("Istid", "Barn", "2010", "Director", "pictureUrl", "trailerUrl");
        Character sid = new Character("Sid", "None", "Dumb", "pictureUrl");
        HashSet<Character> charactersIstid= new HashSet<>();
        charactersIstid.add(sid);
        istid.setCharacters(charactersIstid);

        characterRepository.save(sid);
        movieRepository.save(istid);

        characterRepository.save(bruce);
        characterRepository.save(joker);
        franchiseRepository.save(DC);
        movieRepository.save(batman);
        movieRepository.save(batman2);

    }
}
