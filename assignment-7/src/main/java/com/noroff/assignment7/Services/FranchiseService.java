package com.noroff.assignment7.Services;

import com.noroff.assignment7.Models.Character;
import com.noroff.assignment7.Models.Franchise;
import com.noroff.assignment7.Models.Movie;
import com.noroff.assignment7.Repositories.FranchiseRepository;
import com.noroff.assignment7.Repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class FranchiseService {

    @Autowired
    private FranchiseRepository franchiseRepository;
    @Autowired
    private MovieRepository movieRepository;

    public List<Franchise> getAllFranchises(){
        return franchiseRepository.findAll();
    }


    public Franchise getFranchise(Long id){
        return franchiseRepository.findById(id).get();
    }

    public Franchise addFranchise(Franchise franchise){
        return franchiseRepository.save(franchise);
    }

    public Franchise updateFranchise(Franchise franchise){
        return franchiseRepository.save(franchise);
    }

    public Franchise addMovies(Long id, Integer[] movieIds){
        Franchise franchise = franchiseRepository.getById(id);
        Set<Movie> movies = new HashSet<>();
        for (int movieId : movieIds){
            movies.add(movieRepository.getById((long) movieId));
        }
        franchise.setMovies(movies);
        return franchiseRepository.save(franchise);
    }

    public Set<Movie> getMovies(Long id) {
        return franchiseRepository.findById(id).get().getMovies();
    }


    public Set<Character> getCharacters(Long id) {
        Set<Movie> movies = franchiseRepository.findById(id).get().getMovies();
        Set<Character> characters = new HashSet<>();
        for (Movie movie: movies) {
            characters.addAll(movie.getCharacters());
        }
        return characters;
    }
}
