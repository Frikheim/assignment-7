package com.noroff.assignment7.Services;

import com.noroff.assignment7.Models.Character;
import com.noroff.assignment7.Repositories.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CharacterService {
    @Autowired
    private CharacterRepository characterRepository;

    public List<Character> getAllCharacter(){
        return characterRepository.findAll();
    }

    public Character getCharacterById(Long id){
        return characterRepository.findById(id).get();
    }

    public Character addCharacter(Character character){
        return characterRepository.save(character);
    }

    public Character updateCharacter(Character character){
        return characterRepository.save(character);
    }
}
