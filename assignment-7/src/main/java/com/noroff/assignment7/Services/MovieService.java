package com.noroff.assignment7.Services;

import com.noroff.assignment7.Models.Character;
import com.noroff.assignment7.Models.Movie;
import com.noroff.assignment7.Repositories.CharacterRepository;
import com.noroff.assignment7.Repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class MovieService {
    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private CharacterRepository characterRepository;

    public List<Movie> getAllMovies(){
        return movieRepository.findAll();
    }

    public Movie getMovieById(Long id){
        return movieRepository.findById(id).get();
    }

    public Movie addMovie(Movie movie){
        return movieRepository.save(movie);
    }

    public Movie updateMovie(Movie movie){
        return movieRepository.save(movie);
    }

    public Movie addCharacters(Long id, Integer[] characterIds){
        Movie movie = movieRepository.getById(id);
        Set<Character> characters = new HashSet<>();
        for (int characterId : characterIds){
            characters.add(characterRepository.getById((long) characterId));
        }
        movie.setCharacters(characters);
        return movieRepository.save(movie);
    }

    public Set<Character> getCharacters(Long id) {
        return movieRepository.findById(id).get().getCharacters();
    }
}
