package com.noroff.assignment7.Controllers;

import com.noroff.assignment7.Models.Character;
import com.noroff.assignment7.Services.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/characters")
public class CharacterController {
    @Autowired
    private CharacterService characterService;


    @GetMapping()
    public List<Character> getAllCharacter(){
        return characterService.getAllCharacter();
    }

    @GetMapping("/{id}")
    public Character getCharacterById(@PathVariable Long id){
        return characterService.getCharacterById(id);
    }

    @PostMapping
    public Character addCharacter(@RequestBody Character character){
        return characterService.addCharacter(character);
    }

    @PutMapping
    public Character updateCharacter(@RequestBody Character character){
        return characterService.updateCharacter(character);
    }
}
