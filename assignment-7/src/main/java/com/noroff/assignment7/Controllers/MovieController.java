package com.noroff.assignment7.Controllers;


import com.noroff.assignment7.Models.Character;
import com.noroff.assignment7.Models.Movie;
import com.noroff.assignment7.Services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/movies")
public class MovieController {

    @Autowired
    private MovieService movieService;

    @GetMapping
    public List<Movie> getAllMovies(){
        return movieService.getAllMovies();
    }

    @GetMapping("/{id}")
    public Movie getMovieById(@PathVariable Long id){
        return movieService.getMovieById(id);
    }

    @PostMapping
    public Movie addMovie(@RequestBody Movie movie){
        return movieService.addMovie(movie);
    }

    @PutMapping
    public Movie updateMovie(@RequestBody Movie movie){
        return movieService.updateMovie(movie);
    }

    @PutMapping("/{id}")
    public Movie addCharacters(@PathVariable Long id, @RequestBody Integer[] characterIds){
        return movieService.addCharacters(id, characterIds);
    }

    @GetMapping("/{id}/characters")
    public Set<Character> getCharacters(@PathVariable Long id) {
        return movieService.getCharacters(id);
    }
}
