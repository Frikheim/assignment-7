package com.noroff.assignment7.Controllers;

import com.noroff.assignment7.Models.Character;
import com.noroff.assignment7.Models.Franchise;
import com.noroff.assignment7.Models.Movie;
import com.noroff.assignment7.Services.FranchiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/franchises")
public class FranchiseController {

    @Autowired
    private FranchiseService franchiseService;

    @GetMapping
    public List<Franchise> getAllFranchises(){
        return franchiseService.getAllFranchises();
    }

    @GetMapping("/{id}")
    public Franchise getFranchise(@PathVariable Long id){
        return franchiseService.getFranchise(id);
    }

    @PostMapping
    public Franchise addFranchise(@RequestBody Franchise franchise){
        return franchiseService.addFranchise(franchise);
    }

    @PutMapping
    public Franchise updateFranchise(@RequestBody Franchise franchise){
        return franchiseService.updateFranchise(franchise);
    }

    @PutMapping("/{id}")
    public Franchise addMovies(@PathVariable Long id, @RequestBody Integer[] movieIds){
        return franchiseService.addMovies(id, movieIds);
    }

    @GetMapping("/{id}/movies")
    public Set<Movie> getMovies(@PathVariable Long id) {
        return franchiseService.getMovies(id);
    }

    @GetMapping("/{id}/characters")
    public Set<Character> getCharacters(@PathVariable Long id) {
        return franchiseService.getCharacters(id);
    }

}
